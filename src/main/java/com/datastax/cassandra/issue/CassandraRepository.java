package com.datastax.cassandra.issue;

import com.datastax.driver.core.*;
import io.reactivex.Completable;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by Vitaly Romashkin on 03.06.2017.
 */
@Slf4j
public class CassandraRepository {

    private Session session;

    private Map<String,PreparedStatement> preparedStatementMap;


    public CassandraRepository(Session session, @NonNull Map<String,String> ddlStatements, @NonNull Map<String,String> preparedStatements) {
        this.session = session;
        executeSchema(ddlStatements);
        prepareStatements(preparedStatements);
        setConsistencyLevels();
    }

    private void setConsistencyLevels() {}

    private void executeSchema(Map<String, String> ddlStatements) {
        try {
            session.execute(ddlStatements.get("CREATE_KEYSPACE"));
            session.execute(ddlStatements.get("CREATE_ENTRIES_TABLE"));
            session.execute(ddlStatements.get("CREATE_INDEX_ON_PRIVATE"));

            log.info("cassandra schema was initialized");
        } catch (Throwable e) {
            log.error("could not execute schema",e);
            throw new RuntimeException(e);
        }
    }

    private void prepareStatements(Map<String, String> statements) {
        preparedStatementMap = statements.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> session.prepare(entry.getValue())));
        log.debug("prepared statements were loaded");
    }


    public Completable addAll(List<Entry> entries) {

        log.debug("addEntries: size = {}", entries.size());

        LocalDate localDate = null;

        BatchStatement insertBatchStatement = new BatchStatement(BatchStatement.Type.LOGGED);
        UUID uuid;

        for (Entry entry : entries) {

            localDate = LocalDate.fromMillisSinceEpoch(entry.getCreatedOnTimestamp());
            uuid = UUID.fromString(entry.getUuid());

            insertBatchStatement.add(preparedStatementMap.get("INSERT_NEW_ENTRY").bind(
                    localDate,
                    uuid,
                    entry.getCreatedOnTimestamp(),
                    entry.isPrivate(),
                    entry.getExpiresOnTimestamp(),
                    entry.getContent(),
                    (entry.getTitle() == null ? "" : entry.getTitle()) // cause it's optional
            ));
        }

        return executeStatement(insertBatchStatement);
    }


    public Completable clean() {

        log.debug("clean");

        final Statement truncateMainTable = preparedStatementMap.get("TRUNCATE").bind();

        return executeStatement(truncateMainTable);
    }


    private Completable executeStatement(Statement statement){
        return Completable.fromFuture(session.executeAsync(statement));
    }


}
