package com.datastax.cassandra.issue;

import com.google.common.base.Objects;
import lombok.Data;

/**
 * Created by Vitaly Romashkin on 03.06.2017.
 */
@Data
public class Entry {

    private String uuid;

    private String content;

    private String title;

    private boolean isPrivate;

    private long createdOnTimestamp;

    private long expiresOnTimestamp = 0L;

    private String date; // use in cassandra partition key (ddMMyyyy)

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entry that = (Entry) o;
        return Objects.equal(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid);
    }
}
