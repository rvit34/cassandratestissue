package com.datastax.cassandra.issue;

import lombok.AllArgsConstructor;
import lombok.Value;

/**
 * Created by Vitaly Romashkin on 28.05.2017.
 */
@Value
@AllArgsConstructor
public class Page<T> {

    private int number;

    private int size;

    private T content;

}
