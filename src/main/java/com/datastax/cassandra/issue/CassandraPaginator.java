package com.datastax.cassandra.issue;

import com.datastax.driver.core.*;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.reactivex.Observable;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Vitaly Romashkin on 03.06.2017.
 */
@Slf4j
public class CassandraPaginator  {

    private int pageSize = 10;
    private int fetchSize = pageSize*10;
    private int maxPageNumber = 100;

    private volatile PagingState pagingState = null;
    private volatile ResultSet rs;
    private volatile int currentPage = -1;
    private volatile int requestedPage = -1;

    private Session session;
    private Statement statement;

    private int cacheSize = fetchSize*10;
    private LimitedQueue<Row> cachedRows = new LimitedQueue<>(cacheSize);


    public CassandraPaginator(Session session, Statement statement, int fetchSize) {
        this.session = session;
        this.statement = statement;
        prefetch(statement,fetchSize);
    }

    private void prefetch(Statement statement, int fetchSize){
        setFetchSize(fetchSize);
        rs = session.execute(statement);
        readAvailableRowsToCache(rs);
        pagingState = rs.getExecutionInfo().getPagingState();
    }

    private void setFetchSize(int fetchSize) {
        if (fetchSize < pageSize) throw new IllegalArgumentException(String.format("fetchSize cannot be less than pageSize=%d",pageSize));
        this.fetchSize = fetchSize;
        this.statement.setFetchSize(fetchSize);
        this.cacheSize = fetchSize*10;
        this.cachedRows = new LimitedQueue<>(cacheSize);
    }


    public Page<Observable<Row>> nextPage() {

        Objects.requireNonNull(session);
        Objects.requireNonNull(statement);
        Objects.requireNonNull(rs);

        if ((currentPage + 1) > maxPageNumber){
            throw new IllegalArgumentException(String.format("page cannot be greater than %d", maxPageNumber));
        }

        currentPage = currentPage + 1;
        requestedPage = currentPage;

        Observable<Row> observableRows;
        if (currentPage == 0 && cachedRows.size() == 0){
            observableRows = next(currentPage,session.executeAsync(statement));
        }else{
            observableRows = next(currentPage, Futures.immediateFuture(rs));
        }

        observableRows.doOnComplete(() -> currentPage = requestedPage);

        return new Page(requestedPage,pageSize,observableRows);
    }


    public boolean hasNextPage() {
        return pagingState != null && (currentPage + 1) <= maxPageNumber;
    }


    public Page<Observable<Row>> currentPage(){

        if (currentPage == -1) currentPage = 0;

        Observable<Row> rowObservable;
        if (pageContainsInCache(currentPage)){
            rowObservable = Observable.fromIterable(getCachedRows(currentPage));
        }else{
            requestedPage = currentPage;
            log.debug("current page is not found in cache");
            rowObservable = next(currentPage, session.executeAsync(statement));
        }

        return new Page(
                currentPage,
                pageSize, rowObservable);
    }


    public void setPageSize(int pageSize) {
        if (pageSize <= 0) throw new IllegalArgumentException("pageSize should be positive");
        if (pageSize > fetchSize) throw new IllegalArgumentException(String.format("pageSize cannot be greater than fetchSize=%d",fetchSize));
        this.pageSize = pageSize;
    }


    public void reinit(){
        this.currentPage = -1;
        cachedRows.clear();
        statement.setPagingState(null);
    }

    private boolean pageContainsInCache(int pageNumber){
        int fromRow = pageNumber * pageSize;
        int toRow = fromRow + pageSize;
        int cacheCount = cachedRows.size() + cachedRows.countOfRemovedElements;
        return fromRow <= cacheCount && toRow <= cacheCount && fromRow >= cachedRows.countOfRemovedElements;
    }

    private List<Row> getCachedRows(int pageNumber){

        final List<Row> pageRows = new LinkedList<>();

        final int fromRow = pageNumber*pageSize;
        final int startIndex = fromRow - cachedRows.countOfRemovedElements;
        final int endIndex = startIndex + pageSize;

        int k = 0;
        Iterator<Row> it = cachedRows.iterator();
        while(it.hasNext()){
            Row row = it.next();
            if (k >= startIndex && k < endIndex){
                pageRows.add(row);
            }
            k++;
        }

        return pageRows;
    }

    private int readAvailableRowsToCache(ResultSet rs){
        // how far we can go without triggering the blocking fetch:
        int remainingInPage = rs.getAvailableWithoutFetching(); // async operation
        if (remainingInPage == 0) return 0;

        int k = remainingInPage;
        for (Row row: rs){
            cachedRows.add(row);
            if (--k == 0) break;
        }
        return remainingInPage;
    }

    private Observable<Row> next(final int page, ListenableFuture<ResultSet> future){
        ListenableFuture<List<Row>> result = Futures.transformAsync(future,
                iterateForward(page,requestedPage));
        return Observable.fromFuture(result)
                .flatMap(rows -> Observable.fromIterable(rows));
    }


    private AsyncFunction<ResultSet,List<Row>> iterateForward(int page, final int requestedPage) {
        return rs -> {
            log.debug("result set: {}", rs);

            CassandraPaginator.this.rs = rs;
            int availableRows = readAvailableRowsToCache(rs);
            pagingState = rs.getExecutionInfo().getPagingState();

            if (page == requestedPage && pageContainsInCache(page)){
                return Futures.immediateFuture(getCachedRows(page));
            }

            if (!hasNextPage() && availableRows == 0){
                log.debug("we reach the end of the result set");
                return Futures.immediateFuture(Collections.EMPTY_LIST);
            }

            // go forward
            ListenableFuture<ResultSet> future = rs.fetchMoreResults(); // async operation
            return Futures.transformAsync(future, iterateForward((page+1),requestedPage));
        };
    }

    /**
     * based on wait-free nonblocking implementation
     * @param <E>
     */
    private class LimitedQueue<E> extends ConcurrentLinkedQueue<E> {

        private int maxSize;

        private int countOfRemovedElements = 0;

        public LimitedQueue(int maxSize){
            this.maxSize = maxSize;
        }


        public void add(Collection<? extends E> elements) {
            elements.forEach(e -> this.add(e));
        }

        @Override
        public boolean add(E o) {
            boolean added = super.add(o);
            while (added && size() > maxSize) {
                super.remove();
                countOfRemovedElements++;
            }
            return added;
        }

    }

}
