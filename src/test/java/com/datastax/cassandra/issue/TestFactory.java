package com.datastax.cassandra.issue;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vitaly Romashkin on 03.06.2017.
 */
@Slf4j
public class TestFactory {

    public Cluster createCassandraTestCluster(){
        return Cluster.builder()
                .addContactPoint("127.0.0.1")
                .withPort(9042)
                .build();
    }

    private JsonObject loadCassandraStatements(){
        String statementsFile = "statements.json";
        try {
            return loadConfiguration(statementsFile);
        } catch (IOException e) {
            log.error("unable to load statements from resource {}",statementsFile,e);
            return null;
        }
    }

    public Map<String,String> loadCassandraQueryStatementsToMap(){
        JsonObject statementsJson = loadCassandraStatements();
        JsonObject queryStatements = statementsJson.get("QueryStatements").asObject();
        return  toMap(queryStatements);
                /*
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
                */
    }

    private Map<String,String> toMap(JsonObject statements) {
        Iterator<JsonObject.Member> elements = statements.iterator();
        Map<String,String> map = new HashMap<>(statements.size()+1,1);
        JsonObject.Member element;
        while (elements.hasNext()){
            element = elements.next();
            map.put(element.getName(),element.getValue().asString());
        }
        return map;
    }

    public CassandraRepository getCassandraTestRepository(Session session){

        JsonObject statementsJson = loadCassandraStatements();
        JsonObject ddlStatementsJson = statementsJson.get("DDLStatements").asObject();
        JsonObject queryStatementsJson = statementsJson.get("QueryStatements").asObject();

        Map<String,String> ddlStatements = toMap(ddlStatementsJson);
                /*.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));*/

        Map<String,String> preparedStatements = toMap(queryStatementsJson);
                /*.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,entry -> entry.getValue().toString()));*/


        return new CassandraRepository(session,ddlStatements,preparedStatements);
    }


    public Entry createTestPasteEntry(boolean isPrivate, boolean isExpired){

        Entry entry = new Entry();
        entry.setPrivate(isPrivate);
        entry.setExpiresOnTimestamp(isExpired?System.currentTimeMillis()-10*1000:System.currentTimeMillis()+100*1000);
        entry.setTitle("Test entry");
        entry.setContent("Test: Hello,World!");
        entry.setCreatedOnTimestamp(System.currentTimeMillis());
        entry.setUuid(generateUUID());

        return entry;
    }

    public static String generateUUID(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public List<Entry> generateTestEntries(boolean isPrivate, boolean isExpired, int count, int delay){

        return Observable
                .generate((Consumer<Emitter<Entry>>) pasteEntryEmitter -> {
                    pasteEntryEmitter.onNext(TestFactory.this.createTestPasteEntry(isPrivate, isExpired));
                    pasteEntryEmitter.onComplete();
                })
                .delay(delay, TimeUnit.MILLISECONDS)
                .repeat(count)
                .toList()
                .blockingGet();
    }


    public static JsonObject loadConfiguration(String configFile) throws IOException{
        String configContent = loadContent(TestFactory.class.getResourceAsStream("../../../../"+configFile));
        return Json.parse(configContent).asObject();
    }


    public static String loadContent(@NonNull InputStream is) throws IOException{
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            byte[] buffer  = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = is.read(buffer)) != -1) {
                baos.write(buffer,0,bytesRead);
            }

            return new String(baos.toByteArray(), StandardCharsets.UTF_8);
        } finally {
            if (baos != null){
                baos.close();
            }
            try {
                is.close();
            } catch (IOException e) {}
        }
    }

}
