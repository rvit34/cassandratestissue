package com.datastax.cassandra.issue;

import com.datastax.driver.core.*;
import com.google.common.collect.Lists;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

@RunWith(JUnit4.class)
@Slf4j
public class CassandraPaginatorTest {

    private static TestFactory testFactory;
    private static Cluster cluster;
    private static Session session;

    private static CassandraRepository repository;
    private static CassandraPaginator paginator;

    private static Map<String, String> statements;

    @BeforeClass
    public static void setUp() throws Exception {

        testFactory = new TestFactory();
        cluster = testFactory.createCassandraTestCluster();
        session = cluster.connect();
        repository = testFactory.getCassandraTestRepository(session);
        statements = testFactory.loadCassandraQueryStatementsToMap();

        Statement statement = new SimpleStatement(statements.get("GET_PUBLIC_ENTRIES_DESC_ORDER"));
        //statement.setConsistencyLevel(ConsistencyLevel.SERIAL);

        paginator = new CassandraPaginator(session, statement, 10);
    }


    @Test
    //@Ignore
    public void testThatCurrentPageIsGettingCorrectly() {

        // given:
        paginator.setPageSize(5);
        final List<Entry> testEntries = testFactory.generateTestEntries(false, false, 10, 1);

        final Observable<Entry> result = repository
                                                .addAll(testEntries)
                                                .andThen(Single.fromCallable(paginator::currentPage))
                                                .map(page -> page.getContent())
                                                .flatMapObservable(rowObservable -> rowObservable)
                                                .map(row -> mapToPastEntry().apply(row));

        // when:
        TestObserver observer = result.test();

        // then:
        observer.assertComplete();
        observer.assertNoErrors();

        observer.assertValueCount(5);
        observer.assertValues(Lists.reverse(testEntries.subList(5, 10)).toArray(new Entry[5]));

    }


    @Test
    //@Ignore
    public void testForwardPagination() {

        // given:
        paginator.setPageSize(3);
        final List<Entry> testEntries = testFactory.generateTestEntries(false, false, 15, 1);

        final Observable<Entry> result = repository
                                                .addAll(testEntries)
                                                .andThen(
                                                        Single.concat(
                                                                Single.fromCallable(paginator::nextPage),
                                                                Single.fromCallable(paginator::nextPage),
                                                                Single.fromCallable(paginator::nextPage)
                                                        )
                                                )
                                                .map(page -> page.getContent())
                                                .toObservable().flatMap(rowObservable -> rowObservable)
                                                .map(row -> mapToPastEntry().apply(row));


        // when:
        TestObserver observer = result.test();

        //then:
        observer.assertComplete();
        observer.assertNoErrors();

        observer.assertValueCount(9);
        observer.assertValues(Lists.reverse(testEntries.subList(6, 15)).toArray(new Entry[9]));


    }


    @After
    public void cleanUp() {
        repository.clean().blockingAwait();
        paginator.reinit();
        log.debug("end cleaned up");
    }


    private Function<Row, Entry> mapToPastEntry() {
        return row -> {
            Entry entry = new Entry();
            entry.setUuid(row.getUUID("id").toString());
            entry.setDate(row.getDate("day").toString());
            entry.setCreatedOnTimestamp(row.getTime("created"));
            entry.setTitle(row.getString("title"));
            entry.setContent(row.getString("content"));
            entry.setExpiresOnTimestamp(row.getTime("expires"));
            entry.setPrivate(row.getBool("private"));

            return entry;
        };
    }

}